<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cards;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //busco todos los registros por el método all() de eloquent
        $car=Cards::all();
        return \response($car);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            //Expresiones regulares
            'color' => 'required',
            'cilindraje' => 'required|max:5',
            'potencia' =>'required',
            'peso' => 'required'
        ]);
        $car=Cards::create($request->all());
        return \response($car); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {//si no se encuentra, devuelve error
        $car=Cards::findOrFail($id);
        return \response($car); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //si no se encuentra, devuelve error
        $car=Cards::findOrFail($id)->update($request->all());
        return \response("Carro actualizado"); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cards::destroy($id);
        return \response("El carro ha sido eliminado    "); 
    }
}
