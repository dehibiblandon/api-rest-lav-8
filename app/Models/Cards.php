<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    use HasFactory;
    protected $table="cars";
    protected $primaryKey="serie";
    protected $fillable=array('cilindraje', 'color', 'potencia', 'peso');

}
